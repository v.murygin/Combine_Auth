//
//  SendCodeEntity.swift
//  Refueler
//
//  Created by Boris Vecherkin on 28.01.2021.
//  Copyright © 2021 MOBILNAYA ZAPRAVKA, OOO. All rights reserved.
//

import Foundation

struct SendCodeRequest {
    let phone: String
    
    init(phone: String) {
     
        self.phone = phone
    }
}
