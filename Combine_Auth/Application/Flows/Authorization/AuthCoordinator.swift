
import UIKit

class AuthCoordinator: BaseCoordinator {
    
    private let router: Router
    private let authScreenFactory: AuthScreenFactory
    
      
    init(router: Router, authScreenFactory: AuthScreenFactory) {
        self.router = router
        self.authScreenFactory = authScreenFactory
        
    }
    
    override func start() {
        open()
    }
    
    private func open() {
        
        let screen = authScreenFactory.makeAuthScreen()
        router.setRootModule(screen)
    }
    
}
