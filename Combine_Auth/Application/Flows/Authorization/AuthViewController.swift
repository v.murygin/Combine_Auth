//
//  AuthViewController1.swift
//  Combine_Auth
//
//  Created by Владислав on 18.02.2021.
//

import UIKit
import Combine
import Alamofire
import SwiftKeychainWrapper

class AuthViewController<View: AuthView>: BaseViewController<View> {
    
    typealias ErrorMessage = String
    
    private let loginProvider: LoginProvider
    var cancellables = Set<AnyCancellable>()
    
    init(loginProvider: LoginProvider) {
        self.loginProvider = loginProvider
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rootView.update(with: makeLoginScreenViewInputData(from: loginProvider.state.value))
        
        rootView.events.sink { [weak self] in
            self?.onViewEvent($0)
        }
        .store(in: &cancellables)
        
        loginProvider.state.sink { [weak self] in
            self?.onStateChange($0)
        }
        .store(in: &cancellables)
        
    }
    
    
    func onViewEvent(_ event: LoginScreenViewEvent) {
        switch event {
        
        case .formDidChange(let fromData):
            guard let data = fromData as? LoginScreenViewOutputData else {return}
            LoginCredentials(phone: data.phone, code: data.code)
            loginProvider.update(phone: data.phone, code: data.code)
            
        case .authButtonPressed:
            loginProvider.singIn()
            
        case .codeButtonPressed(let fromData):
            print("codeButtonPressed")

            loginProvider.sendCode()
            
        }
    }
    
    func onStateChange(_ state: LoginProviderState) {
        
        if state.isAuth {
            //    print(state.isAuth)
            print("Вы авторизированы")
        } else {
            rootView.update(with: makeLoginScreenViewInputData(from: state))
        }
    }
    
    func makeLoginScreenViewInputData(from state: LoginProviderState) -> LoginScreenViewInputData {
        
        let crsfToken = state.crsfToken
        let isCanSubmit = !state.crsfToken.isEmpty && !state.isAuth
        let crsfTokenState = state.crsfTokenState
        let errorMessage = state.error.map(makeErrorMessage)
        let resendingAccess = state.resendingAccess
        let textCounter = state.textCounter
        let elementsState = state.elementsState
        
        
        return LoginScreenViewInputData(isCanSubmit: isCanSubmit,
                                        errorMessage: errorMessage,
                                        crsfToken: crsfToken,
                                        crsfTokenState: crsfTokenState,
                                        resendingAccess: resendingAccess,
                                        textCounter: textCounter,
                                        elementsState: elementsState
        )
    }
    
    func makeErrorMessage(from error: Error) -> String {
        
        switch error {
        case let error as ApiClientError where error == .unauthorized:
            return "Invalid username and/or password: You did not provide a valid login."
        default:
            return "Developer mistake. Please try turning it off and on again"
        }
    }
    
}

