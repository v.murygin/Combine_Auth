////
////  ItemDetailsViewController.swift
////  Coordinator Example
////
////  Created by yuriy.p on 16.12.2020.
////
//
//import UIKit
//
//enum MainOutput {
//    case close
//}
//
//class MainViewController: UIViewController, ModuleOutput {
//    
//    typealias Output = MainOutput
//    var moduleOutputHandler: ModuleOutputHandler?
//    
//    var infoLabel: UILabel!
//    
//    var value: String?
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        view.backgroundColor = .blue
//        
//        let barButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(backAction))
//        navigationItem.setLeftBarButton(barButtonItem, animated: false)
//    }
//    
//    @objc func backAction() {
//        moduleOutputHandler?(.close)
//    }
//    
//
//}
