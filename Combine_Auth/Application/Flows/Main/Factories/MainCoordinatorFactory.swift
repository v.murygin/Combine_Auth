////
////  DetailsCoordinatorFactory.swift
////  Coordinator Example
////
////  Created by yuriy.p on 16.12.2020.
////
//
//import UIKit
//
//protocol MainCoordinatorFactory {
//    
//    func makeMainCoordinator(router: NavigationRouter, with value: String) -> Coordinator
//}
//
//class MainCoordinatorFactoryImpl: MainCoordinatorFactory {
//    func makeMainCoordinator(router: NavigationRouter, with value: String) -> Coordinator {
//        MainCoordinator(router: router, detailsValue: value)
//    }
//}
