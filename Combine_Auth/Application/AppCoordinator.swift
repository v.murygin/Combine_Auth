//
//  AppCoordinator.swift
//  Coordinator Example
//
//  Created by yuriy.p on 16.12.2020.
//

import UIKit

class AppCoordinator: BaseCoordinator {

    private let router: Router
    private var childCoordinator: BaseCoordinator?
    private let coordinatorFactory: AuthCoordinatorFactory


    
    init(router: Router, coordinatorFactory: AuthCoordinatorFactory) {
        self.router = router
        self.coordinatorFactory = coordinatorFactory
 
    }
    
    override func start() {
        runInitialFlow()
    }
    
    private func runInitialFlow() {
        childCoordinator = coordinatorFactory.makeAuthCoordinator(router: router)
        childCoordinator?.start()
    }
}
