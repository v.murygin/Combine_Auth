import Combine
import Foundation

protocol LoginProvider {
    var state: CurrentValueSubject<LoginProviderState, Never> { get }
    func singIn()
    func sendCode()
    func update(phone: String, code: String)
}


final class LoginProviderImpl: LoginProvider {
    
    let state = CurrentValueSubject<LoginProviderState, Never>(LoginProviderState.inital)
    
    var sendCodeResponse: SendCodeResponse?
    
    private let authenticator: AuthenticationService
    private var request: AnyCancellable?
    private var cancellable: AnyCancellable?
    
    private var timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    private var count = 0
    private var stopCount = 10
    
    private var labelText = "Запросите код"
       
    
    init(authenticator: AuthenticationService) {
        self.authenticator = authenticator
    }
    
    
    func sendCode() {
        request?.cancel()
        
        self.state.value =  self.state.value.with(resendingAccess: false)
        
        let operation = self.authenticator
            .sendCode(to: SendCodeRequest(phone: self.state.value.phone))
            .receive(on: RunLoop.main)
        
        request = operation.sink( receiveCompletion: {_ in 
            print("sendCode complite")
        }, receiveValue: { sendCodeResponse in
            self.state.value =  self.state.value.with(crsfToken: sendCodeResponse.csrfToken)
           
            if sendCodeResponse.csrfToken != "" {
                self.labelText = "Токен получен"
                
                self.state.value =  self.state.value.with(elementsState:
                                                            ViewElementsState(getSmsButton: true,
                                                                                goButton: false,
                                                                                phoneTextField: true,
                                                                                codeTextField: false,
                                                                                labelText: self.labelText
                ))
            }
        })
        countdownToResend()
        
        
    }
    
    func update(phone: String, code: String) {
        self.state.value =  self.state.value.with(code: code)
        self.state.value =  self.state.value.with(phone: phone)
    }
    
    private func countdownToResend(){
        
        cancellable = timer.sink(receiveValue: {test in
            self.count += 1
         
            self.state.value =  self.state.value.with(textCounter: "Повторный запрос через: \(self.stopCount - self.count)")
            self.state.value =  self.state.value.with(resendingAccess: false)
            
            self.state.value =  self.state.value.with(elementsState:
                                                        ViewElementsState(getSmsButton: true,
                                                                            goButton: false,
                                                                            phoneTextField: true,
                                                                            codeTextField: false,
                                                                            labelText: self.labelText))
            
            if self.count == self.stopCount {
                print("Отсчет закончен")
                self.count = 0
                self.state.value =  self.state.value.with(textCounter:"")
                self.state.value =  self.state.value.with(resendingAccess: true)
                
                self.state.value =  self.state.value.with(elementsState:
                                                            ViewElementsState(getSmsButton: false,
                                                                                goButton: false,
                                                                                phoneTextField: true,
                                                                                codeTextField: false,
                                                                                labelText: self.labelText
                ))
                
                self.cancellable?.cancel()
            }
        })
    }
    
    
    func singIn() {
        
        request?.cancel()
        
        request = authenticator.signIn(with: SignInRequest(phone: self.state.value.phone, code: self.state.value.code, csrfToken: self.state.value.crsfToken)!)
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { [weak self] result in
                guard let self = self, case .failure(let error) = result else { return }
                self.state.value = self.state.value.with(error: error)
            },
            receiveValue: { [weak self] _ in
                guard let self = self else { return }
                self.state.value = self.state.value.with(isAuth: true).with(error: nil)
            })
    }
}

struct ViewElementsState {
    let getSmsButton: Bool
    let goButton: Bool
    let phoneTextField: Bool
    let codeTextField: Bool
    let labelText: String
    
    static let inital = ViewElementsState(
        getSmsButton: false,
        goButton: true,
        phoneTextField: false,
        codeTextField: true,
        labelText: "Запросите код"
    )
}


struct LoginProviderState {
    let phone: String
    let elementsState: ViewElementsState
    let error: Error?
    let isAuth: Bool
    let crsfToken: String
    let crsfTokenState: Bool
    let code: String
    let resendingAccess: Bool
    let textCounter: String

    
    static let inital = LoginProviderState(
        phone: "",
        elementsState: ViewElementsState.inital,
        error: nil,
        isAuth: false,
        crsfToken: "",
        crsfTokenState: false,
        code: "",
        resendingAccess: true,
        textCounter: ""
    )
    
    func with(crsfTokenState: Bool) -> Self {
        LoginProviderState(
            phone: self.phone,
            elementsState: self.elementsState,
            error: self.error,
            isAuth: self.isAuth,
            crsfToken: self.crsfToken,
            crsfTokenState: crsfTokenState,
            code: self.code,
            resendingAccess: self.resendingAccess,
            textCounter: textCounter
        )
    }
    
    func with(textCounter: String) -> Self {
        LoginProviderState(
            phone: self.phone,
            elementsState: self.elementsState,
            error: self.error,
            isAuth: self.isAuth,
            crsfToken: self.crsfToken,
            crsfTokenState: self.crsfTokenState,
            code: self.code,
            resendingAccess: self.resendingAccess,
            textCounter: textCounter
        )
    }
    
    
    func with(crsfToken: String) -> Self {
        LoginProviderState(
            phone: self.phone,
            elementsState: self.elementsState,
            error: self.error,
            isAuth: self.isAuth,
            crsfToken: crsfToken,
            crsfTokenState: self.crsfTokenState,
            code: self.code,
            resendingAccess: self.resendingAccess,
            textCounter: self.textCounter
        )
    }
    
    func with(phone: String) -> Self {
        LoginProviderState(
            phone: phone,
            elementsState: self.elementsState,
            error: self.error,
            isAuth: self.isAuth,
            crsfToken: self.crsfToken,
            crsfTokenState: self.crsfTokenState,
            code: self.code,
            resendingAccess: self.resendingAccess,
            textCounter: self.textCounter
            
        )
    }
    
    func with(resendingAccess: Bool) -> Self {
        LoginProviderState(
            phone: self.phone,
            elementsState: self.elementsState,
            error: self.error,
            isAuth: self.isAuth,
            crsfToken: self.crsfToken,
            crsfTokenState: self.crsfTokenState,
            code: self.code,
            resendingAccess: resendingAccess,
            textCounter: self.textCounter
            
        )
    }
    
    func with(code: String) -> Self {
        LoginProviderState(
            phone: self.phone,
            elementsState: self.elementsState,
            error: self.error,
            isAuth: self.isAuth,
            crsfToken: self.crsfToken,
            crsfTokenState: self.crsfTokenState,
            code: code,
            resendingAccess: self.resendingAccess,
            textCounter: self.textCounter
            
        )
    }
    func with(isAuth: Bool) -> Self {
        LoginProviderState(
            phone: self.phone,
            elementsState: self.elementsState,
            error: self.error,
            isAuth: isAuth,
            crsfToken: self.crsfToken,
            crsfTokenState: self.crsfTokenState,
            code: self.code,
            resendingAccess: self.resendingAccess,
            textCounter: self.textCounter
            
        )
    }
    func with(error: Error?) -> Self {
        LoginProviderState(
            phone: self.phone,
            elementsState: self.elementsState,
            error: error,
            isAuth: self.isAuth,
            crsfToken: self.crsfToken,
            crsfTokenState: self.crsfTokenState,
            code: self.code,
            resendingAccess: self.resendingAccess,
            textCounter: self.textCounter
        )
    }
    func with(elementsState: ViewElementsState) -> Self {
        LoginProviderState(
            phone: self.phone,
            elementsState: elementsState,
            error: self.error,
            isAuth: self.isAuth,
            crsfToken: self.crsfToken,
            crsfTokenState: self.crsfTokenState,
            code: self.code,
            resendingAccess: self.resendingAccess,
            textCounter: self.textCounter
        )
    }
}
