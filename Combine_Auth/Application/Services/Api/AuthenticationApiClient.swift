//
//  AuthenticationManager.swift
//  Refueler
//
//  Created by Владислав Мурыгин on 01.02.2021.
//  Copyright © 2021 MOBILNAYA ZAPRAVKA, OOO. All rights reserved.
//

import Foundation
import Combine

protocol AuthenticationApiClient {
    func sendCode(phone: String) -> AnyPublisher<DataResponse<SendCodeResponse>, Error>
    func signIn(phone: String, code: String, csrfToken: String) -> AnyPublisher<DataResponse<SignInResponse>, Error>
}
