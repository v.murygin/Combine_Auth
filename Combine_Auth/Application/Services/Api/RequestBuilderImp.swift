//
//  RequestBuilderImp.swift
//  Refueler
//
//  Created by Владислав Мурыгин on 01.02.2021.
//  Copyright © 2021 MOBILNAYA ZAPRAVKA, OOO. All rights reserved.
//

import Foundation
import Alamofire


class RequestBuilderImpl: RequestBuilder {
    
    private var host: String = "https://dev-api.refueler.ru:6093"
    private var urlParamters: Parameters = [:]
    private var headerParamters: Parameters = [:]

    
    func build(
        path: String,
        method: HTTPMethod,
        token: String,
        headerParamters: Parameters,
        urlParamters: Parameters,
        jsonParamters: Parameters) -> URLRequest? {
            
        let urlParamters = self.urlParamters.merging(urlParamters, uniquingKeysWith: { _, value in value })
        
        var urlRequestMap = URL(string: self.host)
            .map { $0.appendingPathComponent(path) }

        var urlRequest = urlRequestMap
            .flatMap { try? URLEncoding().encode(URLRequest(url: $0), with: urlParamters)}
        
        if !jsonParamters.isEmpty {
            urlRequest = urlRequest.flatMap {try? JSONEncoding().encode($0, with: jsonParamters)}
        }

        var headers = self.headerParamters
                .merging(headerParamters) { (_, new) in new }
        
        urlRequest?.headers = HTTPHeaders(headers as! [String:String])
        urlRequest?.method = method
        
        return urlRequest
    }
    
}
