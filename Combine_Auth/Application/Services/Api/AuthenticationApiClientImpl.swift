//
//  AuthenticationManagerImpl.swift
//  Refueler
//
//  Created by Владислав Мурыгин on 01.02.2021.
//  Copyright © 2021 MOBILNAYA ZAPRAVKA, OOO. All rights reserved.
//

import Foundation
import Combine

extension ApiClient: AuthenticationApiClient {
    

    func sendCode(phone: String) -> AnyPublisher<DataResponse<SendCodeResponse>, Error>  {
        let request = requestBuilder.build(
            path: "/dev/auth/sendCode",
            method: .post,
            token: "",
            headerParamters: [:],
            urlParamters: [:],
            jsonParamters: ["phone": phone]
        )
        return performRequest(request)
    }
    
    
    func signIn(phone: String, code: String, csrfToken: String) -> AnyPublisher<DataResponse<SignInResponse>, Error> {
        let request = requestBuilder.build(
            path: "/dev/auth/signIn",
            method: .post,
            token: "",
            headerParamters: [:],
            urlParamters: [:],
            jsonParamters: [
                "phone": phone,
                "code": code,
                "csrfToken": csrfToken
            ]
        )
        return performRequest(request)
    }
    
}
