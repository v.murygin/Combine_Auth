//
//  RequestBuilder.swift
//  Refueler
//
//  Created by Владислав Мурыгин on 01.02.2021.
//  Copyright © 2021 MOBILNAYA ZAPRAVKA, OOO. All rights reserved.
//

import Foundation
import Alamofire

protocol RequestBuilder {
  
    func build(
        path: String,
        method: HTTPMethod,
        token: String,
        headerParamters: Parameters,
        urlParamters: Parameters,
        jsonParamters: Parameters) -> URLRequest?
}
