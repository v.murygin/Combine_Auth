//
// Created by Boris Vecherkin on 27.01.2021.
// Copyright (c) 2021 MOBILNAYA ZAPRAVKA, OOO. All rights reserved.
//

import Foundation
import Combine

protocol AuthenticationService {
    
    var isLoggedIn: Bool { get }

    func sendCode(to: SendCodeRequest) -> AnyPublisher<SendCodeResponse, Error> 
    func signIn(with credentials: SignInRequest) -> AnyPublisher<Void, Error>
}
