//
//  AuthenticationServiceImpl.swift
//  Refueler
//
//  Created by Boris Vecherkin on 27.01.2021.
//  Copyright © 2021 MOBILNAYA ZAPRAVKA, OOO. All rights reserved.
//

import Foundation
import Combine

class AuthenticationServiceImpl: AuthenticationService {

    var isLoggedIn: Bool { sessionRepository.sessionId != nil && sessionRepository.sessionId != "" }
    
    private let sessionRepository: SessionRepository
    private let authenticationManager: AuthenticationApiClient
    private var request: AnyCancellable?
    
    init(
        authenticationManager: AuthenticationApiClient,
        sessionRepository: SessionRepository
    ) {
        
        self.sessionRepository = sessionRepository
        self.authenticationManager = authenticationManager
    }
    
    func sendCode(to: SendCodeRequest) -> AnyPublisher<SendCodeResponse, Error> {
        return self.authenticationManager
                .sendCode(phone: to.phone)
                .map { response in
                    response.data
                }
                .eraseToAnyPublisher()
    }
    
    
    func signIn(with credentials: SignInRequest) -> AnyPublisher<Void, Error> {
        request?.cancel()
        
        let operation = authenticationManager.signIn(phone: credentials.phone, code: credentials.code, csrfToken: credentials.csrfToken)
            .multicast { PassthroughSubject() }
            .autoconnect()
        
        request = operation.sink(
            receiveCompletion: toVoid,
            receiveValue: { [weak sessionRepository] result in
                sessionRepository?.sessionId  = result.data.accessToken.value
                print("sessionId :\(sessionRepository?.sessionId!)")
        })
        
        return operation.map(toVoid).eraseToAnyPublisher()
     }
}


struct LoginCredentials {
    let phone: String
    let code: String
    
    init?(phone: String, code: String) {
        guard phone.count >= 10 && code.count >= 3 else { return nil }
        
        self.phone = phone
        self.code = code
    }
}
