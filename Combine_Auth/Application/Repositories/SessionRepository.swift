//
//  SessionRepository.swift
//  Refueler
//
//  Created by Boris Vecherkin on 27.01.2021.
//  Copyright © 2021 MOBILNAYA ZAPRAVKA, OOO. All rights reserved.
//

import Foundation

protocol SessionRepository: AnyObject {
    var sessionId: String? { get set }
}
